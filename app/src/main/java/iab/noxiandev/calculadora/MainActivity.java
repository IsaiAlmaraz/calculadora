package iab.noxiandev.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

//    TextView vResultado;
    String operador, mostrar, validar;
    Double num1, num2, res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //-- TextView de visualización
        TextView resultado = (TextView) findViewById(R.id.textResultados);


        //--------------  Botones números  ---------------------------------
        Button uno = (Button) findViewById(R.id.btn_1);
        uno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "1";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button dos = (Button) findViewById(R.id.btn_2);
        dos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "2";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button tres = (Button) findViewById(R.id.btn_3);
        tres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "3";    //Se le asigna el valor del boton
                resultado.setText(mostrar);//mostrar en textresultado
            }
        });

        Button cuatro = (Button) findViewById(R.id.btn_4);
        cuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "4";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button cinco = (Button) findViewById(R.id.btn_5);
        cinco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "5";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button seis = (Button) findViewById(R.id.btn_6);
        seis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "6";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button siete = (Button) findViewById(R.id.btn_7);
        siete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "7";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button ocho = (Button) findViewById(R.id.btn_8);
        ocho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "8";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button nueve = (Button) findViewById(R.id.btn_9);
        nueve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "9";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        Button cero = (Button) findViewById(R.id.btn_cero);
        cero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + "0";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

        //-- Punto
        Button punto = (Button) findViewById(R.id.btn_punto);
        punto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = resultado.getText().toString(); //contiene lo que textresultado tenga
                mostrar = mostrar + ".";    //Se le asigna el valor del boton
                resultado.setText(mostrar); //mostrar en textresultado
            }
        });

//-- Botones de operaciones
        Button suma = (Button) findViewById(R.id.btn_suma);
        suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar = resultado.getText().toString();
                if (validar.equals("")){
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else{
                    num1 = Double.parseDouble(validar);
                    operador = "+";
                    resultado.setText("");
                }
            }
        });

        Button resta = (Button) findViewById(R.id.btn_resta);
        resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar = resultado.getText().toString();
                if (validar.equals("")){
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else{
                    num1 = Double.parseDouble(validar);
                    operador = "-";
                    resultado.setText("");
                }
            }
        });

        Button multiplicacion = (Button) findViewById(R.id.btn_multiplicacion);
        multiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar = resultado.getText().toString();
                if (validar.equals("")){
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else{
                    num1 = Double.parseDouble(validar);
                    operador = "*";
                    resultado.setText("");
                }
            }
        });

        Button division = (Button) findViewById(R.id.btn_division);
        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar = resultado.getText().toString();
                if (validar.equals("")){
                    Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                } else{
                    num1 = Double.parseDouble(validar);
                    operador = "/";
                    resultado.setText("");
                }
            }
        });

        Button raiz = (Button) findViewById(R.id.btn_raiz);
        raiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    num1 = Double.parseDouble(resultado.getText().toString());
                    if (num1 == null){
                        Toast.makeText(MainActivity.this, "Ingresa un numero", Toast.LENGTH_LONG).show();
                    } else {
                        res = Math.sqrt(num1);
                        resultado.setText(String.valueOf(res));
                    }
                } catch (NumberFormatException nfe) {
                    resultado.setText("Error");
                }

            }
        });

        //-- Igual
        Button igual = (Button) findViewById(R.id.btn_igual);
        igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    num2 = Double.parseDouble(resultado.getText().toString());
                    if (operador.equals("+")) {
                        res = num1 + num2;
                    } else if (operador.equals("-")) {
                        res = num1 - num2;
                    } else if (operador.equals("*")) {
                        res = num1 * num2;
                    } else if (operador.equals("/")) {
                        try {
                            res = num1 / num2;
                        }catch (NumberFormatException nfe){
                            resultado.setText("Error");
                        }

                    }

                resultado.setText(""+res);
            }
        });

        //-- Boton limpiar
        Button limpiar = (Button) findViewById(R.id.btn_limpiar);
        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar = ""; //se le asigna un espacio vacio
                resultado.setText(mostrar); //mostrar en la etiqueta
            }
        });
    }
}